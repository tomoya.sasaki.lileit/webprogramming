package util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class PasswordEncorder {

  // ハッシュ生成前にバイト配列に書き換える際のCharaset
  private static Charset charset = StandardCharsets.UTF_8;
  // ハッシュアルゴリズム
  private static final String ALGORITHM = "MD5";

  /**
   * パスワード暗号化
   *
   * @param password
   * @return 暗号化されたパスワード
   */
  public static String encordPassword(String password) {
    // 暗号化後のパスワードを格納する変数
    String encodedPass = "";

    try {
      // ハッシュ生成処置
      byte[] bytes = MessageDigest.getInstance(ALGORITHM).digest(password.getBytes(charset));
      encodedPass = DatatypeConverter.printHexBinary(bytes);

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return encodedPass;
  }
}