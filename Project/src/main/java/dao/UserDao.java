package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 * ユーザテーブル用のDao
 *
 * @author shirasawa
 */
public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を取得する：DB検索処理
   *
   * @param loginId
   * @param password
   * @return ユーザー情報
   */
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // レコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
     return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * 全てのユーザ情報を取得する:DB全検索処理
   *
   * @return 全ユーザー情報一覧
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SQL(管理者以外、出力)
      String sql = "SELECT * FROM user WHERE is_admin = false ";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  /**
   * ユーザー新規登録：DB登録処理
   *
   * @param loginId
   * @param name
   * @param birthdate
   * @param password
   * @return なし
   */
  public void insert(String loginId, String name, String birthdate, String password) { // TODO
                                                                                       // birthdateの型
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "INSERT INTO user (login_id, name, birth_date, password, create_date, update_date)VALUES(?,?,?,?,now(),now())";

      stmt = conn.prepareStatement(sql);
      stmt.setString(1, loginId);
      stmt.setString(2, name);
      stmt.setString(3, birthdate);
      stmt.setString(4, password);

      // SQL実行
      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * ユーザー更新 初期表示：DB検索処理
   *
   * @param id
   * @return 更新するユーザー情報
   */
  public User findById(int id) {
    Connection conn = null;
    User user = null;
    PreparedStatement stmt = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE id = ?";
      stmt = conn.prepareStatement(sql);

      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      String loginid = rs.getString("login_id");
      String Name = rs.getString("name");
      Date birthdate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createdate = rs.getTimestamp("create_date");
      Timestamp updatedate = rs.getTimestamp("update_date");

      user = new User(id, loginid, Name, birthdate, password, isAdmin, createdate, updatedate);

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        // コネクションインスタンスがnullでない場合、クローズ処理を実行
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    return user;
  }

  /**
   * ユーザーデータの更新(password更新なし):DB更新処理
   *
   * @param id
   * @param id
   * @param name
   * @param birthdate
   * @return なし
   */
  public void updateExcludePassword(int id, String name, Date birthdate) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE id = ?";

      stmt = conn.prepareStatement(sql);
      stmt.setString(1, name);
      stmt.setDate(2, birthdate);
      stmt.setInt(3, id);

      // SQL実行
      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * ユーザーデータの更新(password更新あり):DB更新処理
   *
   * @param id
   * @param name
   * @param password
   * @param birthdate
   * @return なし
   */
  public void update(int id, String name, String password, Date birthdate) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql =
          "UPDATE user SET name = ?, password = ?, birth_date = ?, update_date = now() WHERE id = ?";

      stmt = conn.prepareStatement(sql);
      stmt.setString(1, name);
      stmt.setString(2, password);
      stmt.setDate(3, birthdate);
      stmt.setInt(4, id);

      // SQL実行
      stmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * ログインIDが登録されているか確認する
   *
   * @param loginId
   * @return boolean
   */
  public boolean existUser(String loginId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      if (rs.next()) {
        return true;
      }

      return false;

    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return false;
        }
      }
    }
  }

  /**
   * ユーザー削除:DB削除処理
   *
   * @param id
   * @return なし
   */
  public void delete(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM user WHERE id =?;";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * ユーザーデータの検索：DB検索処理
   *
   * @param loginId
   * @param name
   * @param startDate
   * @param endDate
   * @return 検索条件に合うユーザー一覧
   */
  public List<User> search(String loginId, String name, String startDate, String endDate) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      /* ***SQL文作成 start*** */
      StringBuilder sql = new StringBuilder("SELECT * FROM user WHERE is_admin = false ");
      List<String> parameters = new ArrayList<String>();

      // ログインIDが入力されていたらsqlに追加
      if (!(loginId.equals(""))) {
        sql.append(" AND login_id = ? ");
        parameters.add(loginId);
      }
      // 名前が入力されていたらsqlに追加
      if (!(name.equals(""))) {
        sql.append(" AND name LIKE ? ");
        parameters.add("%" + name + "%");
      }

      /* ***検索日付範囲*** */
      // 始まりの日付が入力されていたらsqlに追加
      if (!(startDate.equals(""))) {
        sql.append(" AND birth_date >= ? ");
        parameters.add(startDate);
      }
      // 終わりの日付が入力されていたらsqlに追加
      if (!(endDate.equals(""))) {
        sql.append(" AND birth_date <= ? ");
        parameters.add(endDate);
      }
      /* ***SQL文作成 end*** */

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql.toString());
      for (int i = 0; i < parameters.size(); i++) {
        pStmt.setString(i + 1, parameters.get(i));
      }

      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        String resultLoginid = rs.getString("login_id");
        String resultName = rs.getString("name");
        Date resultBirthDate = rs.getDate("birth_date");
        String resultPassword = rs.getString("password");
        boolean resultIsAdmin = rs.getBoolean("is_admin");
        Timestamp resultCreateDate = rs.getTimestamp("create_date");
        Timestamp resultUpdateDate = rs.getTimestamp("update_date");
        User user = new User(id, resultLoginid, resultName, resultBirthDate, resultPassword,
            resultIsAdmin, resultCreateDate, resultUpdateDate);

        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }
}
