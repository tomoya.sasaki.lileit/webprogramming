package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/** Servlet implementation class UserListServlet */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserListServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    //セッションスコープにセットしたユーザ情報の有無確認
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");
    
    //セッションスコープにセットしたユーザの情報がない場合はLoginServletにリダイレクト
    if (user == null) {
     response.sendRedirect("LoginServlet");
      return;
      
    }
    // ユーザ一覧情報を取得できた場合
    UserDao userDao = new UserDao();
    List<User> userList = userDao.findAll();

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("userList", userList);

    // ユーザ一覧のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

  //ユーザ情報を追加登録
    UserDao userDao = new UserDao();
    
 // 検索条件取得
    String loginId = request.getParameter("user-loginid");
    String name = request.getParameter("user-name");
    String startDate = request.getParameter("date-start");
    String endDate = request.getParameter("date-end");

    // 検索処理
    List<User> userList = userDao.search(loginId, name, startDate, endDate);

 // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("userList", userList);
    request.setAttribute("loginId",loginId);
    request.setAttribute("name",name);
    request.setAttribute("startDate",startDate);
    request.setAttribute("endDate",endDate);

 // ユーザ一覧画面へ
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);

  }
}
