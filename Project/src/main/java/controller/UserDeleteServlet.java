package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
	    throws ServletException, IOException {
		
		//セッション
	     HttpSession session = request.getSession();
	     
	     //ログインユーザ情報の取得
	     User loginUser = (User) session.getAttribute("userInfo");
	     
	   //取得できなかったらリダイレクト
	     if(loginUser==null) {
	     response.sendRedirect("LoginServlet");
	     return;
	     }
	     
	     // リクエストパラメータの入力項目を取得
	     int id = Integer.valueOf(request.getParameter("id"));
	     
	     //UserDaoのfindByIdメソッドを呼び出す
	     User user = new UserDao().findById(id);
	     
	     //findByIdメソッドの戻り値をリクエストスコープにセットする
	     request.setAttribute("user",user);
	     
	     //jspにフォワードする
	     RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
	     dispatcher.forward(request, response);
	     return;
	    }
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
	    throws ServletException, IOException {
		 // idを取得
	    String id = request.getParameter("user-id");

	    UserDao dao = new UserDao();
	    
	 // 削除処理
	    dao.delete(Integer.valueOf(id));
	    
	    // ユーザー一覧へ遷移
	    response.sendRedirect("UserListServlet");
	    return;

	}

}
