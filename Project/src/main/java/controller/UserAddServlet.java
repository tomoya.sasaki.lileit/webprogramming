package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  //セッション
	  HttpSession session = request.getSession();
	  
	  //ログインユーザ情報の取得
	  User loginUser = (User) session.getAttribute("userInfo");
	  
	  //取得できなかったらリダイレクト
	  if(loginUser==null) {
	  response.sendRedirect("LoginServlet");
	  return;
	  }
	  //取得できたらフォワード
	  RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	    // リクエストパラメータの文字コードを指定
	    request.setCharacterEncoding("UTF-8");

	  //ユーザ情報を追加登録
        UserDao userDao = new UserDao();
        
	    // リクエストパラメータの入力項目を取得
	    String loginId = request.getParameter("user-loginid");
	    String name = request.getParameter("user-name");
        String birthDate = request.getParameter("birth-date");
	    String password = request.getParameter("password");
	    String passwordConfirm = request.getParameter("password-confirm");
	    
	    if(!isInputAll(loginId,name,birthDate,password,passwordConfirm)||!(password.equals(passwordConfirm))
	        ||userDao.existUser(loginId)) {
	      
	      // エラーメッセージ
          request.setAttribute("errMsg", "入力された情報は正しくありません");
	    
          // エラー後でもリクエストスコープに正しい入力情報を残しておく
          request.setAttribute("inputloginId", loginId);
          request.setAttribute("name", name);
          request.setAttribute("birthDate", birthDate);
          
          //取得できたらフォワード
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
          dispatcher.forward(request, response);
          return;
          
	    }
	    /*新規登録できたらパスワードを暗号化する処理
	    String encordPassword = PasswordEncorder.encordPassword(password);
	   
	    
	    userDao.insert(loginId, name, birthDate ,encordPassword);*/
	    
	    userDao.insert(loginId, name, birthDate ,password);
	    
	    //追加登録できたらリダイレクト
	    response.sendRedirect("UserListServlet");
	}

  private boolean isInputAll(String loginId, String name, String birthDate, String password,
      String passwordConfirm) {
    // TODO 自動生成されたメソッド・スタブ
    return !(loginId.equals("") || password.equals("") || passwordConfirm.equals("") || name.equals("")
        || birthDate.equals(""));

  }

}
