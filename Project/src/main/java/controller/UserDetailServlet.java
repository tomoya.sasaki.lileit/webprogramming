package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/** Servlet implementation class UserDetailServlet */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserDetailServlet() {
    super();
  }
  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
  //セッションスコープにセットしたユーザ情報の有無確認
  HttpSession session = request.getSession();
  User loginUser =(User)session.getAttribute("userInfo");
  
  //セッションスコープにセットしたユーザの情報がない場合はLoginServletにリダイレクト
  if (loginUser == null) {
    response.sendRedirect("LoginServlet");
     return ;
  }
     
     // リクエストパラメータの入力項目を取得
     int id = Integer.valueOf(request.getParameter("id"));
  
     //UserDaoのfindByIdメソッドを呼び出す
     UserDao Dao = new UserDao();
     User user = Dao.findById(id);
     
     //findByIdメソッドの戻り値をリクエストスコープにセットする
     request.setAttribute("user",user);
     
     //userDetail.jspにフォワードする
     RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
     dispatcher.forward(request, response);
     return;
  
  }
}