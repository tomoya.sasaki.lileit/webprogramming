package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/** Servlet implementation class UserUpdateServlet */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserUpdateServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    /**** ログインの有無確認 start ****/
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("userInfo");

    // ログイン情報がない場合
    if (loginUser == null) {
      // セッションがなかったらログイン画面へ
      response.sendRedirect("LoginServlet");
      return;
    }
    /**** ログインの有無確認 end ****/

    // URLパラメータからidを取得
    int id = Integer.valueOf(request.getParameter("id"));

    UserDao dao = new UserDao();

    // 取得したIDに紐づく情報を取得
    User user = dao.findById(id);

    // 更新画面に表示する情報
    request.setAttribute("user", user);

    // 更新画面へ
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);
    return;
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定(文字列取得のため)
    request.setCharacterEncoding("UTF-8");

    User user = new User();
    UserDao userDao = new UserDao();

    int id = Integer.valueOf(request.getParameter("user-id")); // これをキーにして更新する、loginIdはやや信頼に劣る
    // 更新画面に入力された情報を取得
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");
    String confirmPassword = request.getParameter("password-confirm");
    String name = request.getParameter("user-name");
    String birthdateStr = request.getParameter("birth-date");
    // 生年月日をStringからDateへ変換
    Date birthdate = birthdateStr.equals("") ? null : Date.valueOf(birthdateStr);


    /**** ユーザー名、生年月日が空欄、passwordが間違っている場合 ****/
    if (name.equals("") || birthdate == null || !password.equals(confirmPassword)) {
      // エラーメッセージと更新画面へ表示する情報をセットする
      request.setAttribute("errMsg", "入力された内容は正しくありません");

      // 入力した値を再度表示するために値をセットする
      user.setId(id);
      user.setLoginId(loginId);
      user.setName(name);
      user.setBirthDate(birthdate);

      request.setAttribute("user", user);

      // 更新画面へ
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
      return;

    }
    /*新規登録できたらパスワードを暗号化する処理
    String encordPassword = PasswordEncorder.encordPassword(password);
    
    // 更新処理
    userDao.update(id, name, encordPassword, birthdate);*/
   
    userDao.update(id, name, password, birthdate);

    // 更新画面へ遷移
    response.sendRedirect("UserListServlet");
  }
}

