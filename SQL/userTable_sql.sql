﻿create database usermanagement DEFAULT CHARACTER SET utf8;
use usermanagement;
create table user(
    id SERIAL primary key,
    login_id varchar(255) UNIQUE NOT NULL,
    name varchar(255) NOT NULL,
    birth_date DATE NOT NULL,
    password varchar(255) NOT NULL,
    is_admin boolean NOT NULL default false,
    create_date DATETIME NOT NULL,
    update_date DATETIME NOT NULL);
insert into user(
    login_id,
    name,
    birth_date,
    password,
    is_admin,
    create_date,
    update_date
)values(
    'admin',
    '管理者',
    '2023-03-20',
    'password',
    true,
    now(),
    now());

INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user03',
    '一般3',
    '2001-12-31',
    'password',
    now(),
    now()
);

SELECT * FROM user WHERE login_id = 'admin' and password = 'password';
SELECT * FROM user WHERE login_id = 'admin' and password = 'hoge';
